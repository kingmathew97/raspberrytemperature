
import { Component, OnInit } from '@angular/core';
import { FirebaseService } from './services/firebase.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'temperature';
  temp = '28';

  constructor(
    public firebaseService: FirebaseService
  ) { }

  ngOnInit() {
    this.firebaseService.getTemperature()
      .subscribe(result => {
        this.temp = result[0].payload.doc.data()['temperature'.toString()] + ' ℃';
      });
  }
}
