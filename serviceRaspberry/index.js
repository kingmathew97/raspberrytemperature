var cors = require('cors');
const express = require('express');
const bodyparser = require('body-parser');
var app = express();
var firebase = require('firebase-admin');
var serviceAccount = require('./accountKey.json');

app.use(cors());
app.use(bodyparser.json());


firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount)
});

var db = firebase.firestore();

app.listen(3000, () => console.log('Express server is runnig at port no : 3000'));

app.get('/', (req, res) => {
    db.collection('data').get()
        .then((snapshot) => {
            snapshot.forEach((doc) => {
                res.send(doc.data().temperature);
            });
        })
        .catch((err) => {
            res.send('Error getting documents ' + err);
        });
});

app.post('/temperature', (req, res) => {
    let request = req.body;
    temp = request.temp;
    var docRef = db.collection('data').doc('Xb3HJKUG4ZKx4PPO7jyy');
    docRef.set({
      temperature: temp
    });
    res.send(true);
});